<?php
use App\Http\Controllers\AdminController;
use App\Http\Controllers\fasilitasController;
use App\Http\Controllers\transaksi_pulangController;
use App\Http\Controllers\transaksi_inap;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('index');
});
Route::get('/login', function () {
    return view('login');
});
Route::get('/register', function () {
    return view('register');
});


//crud tamu
Route::resource('tamu', 'AdminController');

//crud admin
Route::resource('admin', 'AdminController');

//crud resepsionis
Route::resource('resepsionis', 'AdminController');

//crud Transaksi
Route::resource('transaksi', 'AdminController');
//crud Transaksi
Route::resource('transaksi_inap', 'transaksi_inap');

//crud Transaksi
Route::resource('transaksi_pulang', 'transaksi_pulangController');

//crud Fasilitas
Route::resource('fasilitas', 'fasilitasController');

//crud Harga
Route::resource('harga', 'AdminController');

//crud kamar
Route::resource('kamar', 'AdminController');

//crud profil
Route::resource('profil', 'AdminController');

//crud user
Route::resource('user', 'AdminController');
