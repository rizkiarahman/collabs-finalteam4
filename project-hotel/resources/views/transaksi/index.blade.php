@include('layout.componen.navbar')
@section('judul')
  Halaman Index Hotel
@endsection
@section('content')

<div class="card" style="width: 18rem;">
  <div class="card-body">
    <h5 class="card-title">Bagaimana saya bisa memesan hotel?</h5>
    <p class="card-text">Proses pemesanan hotel radissonhotel.com mudah dan dengan sekali klik saja. Pilih tujuan Anda; masukkan tanggal check-in dan check-out. Lalu klik "Cari sekarang!" 
      dan daftar pilihan Anda akan ditampilkan.</p>
    <a href="#" class="btn btn-primary">Cari Sekarang!</a>
  </div>
</div>

@endsection