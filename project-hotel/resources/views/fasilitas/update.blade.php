@extends('layout.master-2')
@section('judul')
  Halaman Update Tamu
@endsection
@section('content')

<form action="/fasilitas/{{$fasilitas->id}}" method="POST">
  @csrf
  @method('put')


    <div class="mb-3">
        <label class="form-label">Jumlah</label>
        <input type="text" name="jumlah_kamar" value="{{old ('jumlah_kamar',$fasilitas->jumlah_kamar)}}"class="form-control">
    </div>
    <div class="mb-3">
        <label class="form-label">Tipe Kamar</label>
        <input type="text" name="type_kamar" value="{{old ('tipe_kamar',$fasilitas->tipe_kamar)}}" class="form-control">
    </div>
    <div class="mb-3">
        <label class="form-label">Fasilitas</label>
        <input type="text" name="fasilitas_kamar" value="{{old ('fasilitas_kamar',$fasilitas->fasilitas_kamar)}}" class="form-control">
    </div>
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection