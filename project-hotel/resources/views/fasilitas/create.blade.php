@extends('layout.master-2')
@section('judul')
  Halaman Tambah Tamu
@endsection
@section('content')

<form action="/fasilitas" method="POST">
    @csrf
    <div class="mb-3">
        <label class="form-label">Jumlah</label>
        <input type="text" name="jumlah_kamar" class="form-control">
    </div>
    <div class="mb-3">
        <label class="form-label">Tipe Kamar</label>
        <input type="text" name="type_kamar" class="form-control">
    </div>
    <div class="mb-3">
        <label class="form-label">Fasilitas Kamar</label>
        <input type="text" name="fasilitas_kamar" class="form-control">
    </div>

    <br> <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection