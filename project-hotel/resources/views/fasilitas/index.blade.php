@extends('layout.master-2')
@section('judul')
Halaman Daftar Tamu
@endsection

@push('scripts')

<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>

@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.css"/>
@section('content')
<a href="/fasilitas/create" class="btn btn-primary btn-sm mb-4">Tambah Tamu</a>

<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Jumlah Kamar</th>
      <th scope="col">Tipe Kamar</th>
      <th scope="col">Fasilitas Kamar</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($fasilitas as $key => $item)
    <tr>
      <td>{{$key + 1}}</td>
      <td>{{ $item->jumlah_kamar }}</td>
      <td>{{ $item->type_kamar }}</td>
      <td>{{ $item->fasilitas_kamar }}</td>
      <td>
        <form action="/fasilitas/{{$item->id}}" method="POST">
          @csrf
          @method('delete')
        <a href="/fasilitas/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
        <a href="/fasilitas/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>       
          <input type="submit" value="Delete" class="btn btn-danger btn-sm"> 
        </form>
      </td>
    </tr>
    @empty
      <tr>
        <td>Tidak ada di table Tamu</td>  
      </tr>   
    @endforelse
  </tbody>
</table>
   

@endsection