@extends('layout.master-2')
@section('judul')
  Halaman Update Tamu
@endsection
@section('content')

<form action="/transaksi_pulang/{{$transaksi_pulang->id}}" method="POST">
  @csrf
  @method('put')


    <div class="mb-3">
        <label class="form-label">Tanggal Checkout</label>
        <input type="text" name="tgl_checkout" value="{{old ('tgl_checkout',$transaksi_pulang->tgl_checkout)}}"class="form-control">
    </div>
    
  
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection