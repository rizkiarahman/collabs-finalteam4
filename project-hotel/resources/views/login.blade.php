<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('landing/css/bootstrap.css') }}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('landing/vendors/linericon/style.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/vendors/owl-carousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/vendors/lightbox/simpleLightbox.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/vendors/nice-select/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/vendors/animate-css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/vendors/popup/magnific-popup.css') }}">
    <!-- main css -->
    <link rel="stylesheet" href="{{ asset('landing/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/css/responsive.css') }}">
    <title>Login</title>
</head>

<body>
  <div class="d-flex justify-content-center m-auto pt-3 p-5 mb-5">
    <form action="">
      <div class="card" style="width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">LOGIN</h5><hr>
          <div class="mt-10">
            <input type="email" name="email" placeholder="Place your email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'example@enail.com'" required="" class="single-input">
          </div>
          <div class="mt-10">
            <input type="password" name="password" placeholder="Input your pasword" onfocus="this.placeholder = ''" onblur="this.placeholder = 'password'" required="" class="single-input">
          </div>
          <div class="d-flex justify-content-end"><a href="/register">Register?</a></div>
          <input type="submit" value="Login" class="genric-btn info radius">
        </div>
      </div>
      <span class="d-flex justify-content-end">
        <a href="/" class="genric-btn info-border circle arrow mt-2  ">Back to home</a>
      </span>
        </form>
     </div>
    </div>
</body>
</html>