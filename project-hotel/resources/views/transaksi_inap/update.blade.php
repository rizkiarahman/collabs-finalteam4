@extends('layout.master-2')
@section('judul')
  Halaman Update Tamu
@endsection
@section('content')

<form action="/transaksi_inap/{{$transaksi->id}}" method="POST">
  @csrf
  @method('put')


    <div class="mb-3">
        <label class="form-label">Reservasi</label>
        <input type="text" name="reservasi" value="{{old ('reservasi',$transaksi->reservasi)}}"class="form-control">
    </div>
    <div class="mb-3">
        <label class="form-label">Tanggal Checkin</label>
        <input type="text" name="tgl_checkin" value="{{old ('tgl_checkin',$transaksi->tgl_checkin)}}" class="form-control">
    </div>
    <div>
        <label class="form-label">transaksi pulang</label>
        <select name="transaksi_pulang_id" class="form-control">
        <option value="">Transaksi pulang</option>
            @forelse($transaksi_pulang as $item)
            @if($item->id===$transaksi_inap->transaksi_pulang_id)
            <option value="{{$item->id}}" selected>{{$item->tgl_checkout}}</option>
            @else
            <option value="{{$item->id}}">{{$item->tgl_checkout}}</option>
            @endif
            @empty
            <option value="">Tidak ada</option>
            @endforelse
        </select>
        <select name="fasilitas" class="form-control">
        <option value="">Fasilitas Tipe Kamar ---</option>
            @forelse($fasilitas as $item)
            @if($item->id===$transaksi_inap->fasilitas_id)
            <option value="{{$item->id}}" selected>{{$item->type_kamar}}</option>
            @else
            <option value="{{$item->id}}">{{$item->type_kamar}}</option>
            @endif
            @empty
            <option value="">Tidak ada</option>
            @endforelse
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection