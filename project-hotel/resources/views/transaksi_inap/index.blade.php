@extends('layout.master-2')
@section('judul')
Halaman Daftar Tamu
@endsection

@push('scripts')

<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>

@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.css"/>
@section('content')
<a href="/tamu/create" class="btn btn-primary btn-sm mb-4">Tambah Tamu</a>

<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Reservasi</th>
      <th scope="col">Tanggal Checkin</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($transaksi as $key => $item)
    <tr>
      <td>{{$key + 1}}</td>
      <td>{{ $item->reservasi }}</td>
      <td>{{ $item->tgl_checkin }}</td>
      <td>
        <form action="/transaksi_inap/{{$item->id}}" method="POST">
          @csrf
          @method('delete')
        <a href="/transaksi_inap/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
        <a href="/transaksi_inap/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>       
          <input type="submit" value="Delete" class="btn btn-danger btn-sm"> 
        </form>
      </td>
    </tr>
    @empty
      <tr>
        <td>Tidak ada di table Tamu</td>  
      </tr>   
    @endforelse
  </tbody>
</table>