@extends('layout.master-2')
@section('judul')
  Halaman Tambah Tamu
@endsection
@section('content')

<form action="/transaksi_inap" method="POST">
    @csrf
    <div class="mb-3">
        <label class="form-label">Reservasi</label>
        <input type="text" name="reservasi" class="form-control">
    </div>
    <div class="mb-3">
        <label class="form-label">Tanggal Checkin</label>
        <input type="text" name="tgl_checkin" class="form-control">
    </div>
    <div>
        <label class="form-label">Transaksi Pulang</label>
        <select name="transaksi_pulang_id" class="form-control">
            <option value="">Transaksi Pulang-</option>
            @forelse($Transaksipulang as $item)
            <option value="{{$item->id}}">{{$item->tgl_checkout}}</option>
            @empty
            <option value="">Tidak ada</option>
            @endforelse
        </select>
    </div>
    <div>
        <label class="form-label">Fasilitas</label>
        <select name="fasilitas" class="form-control">
            <option value="">Fasilitas-</option>
            @forelse($fasilitas as $item)
            <option value="{{$item->id}}">{{$item->type_kamar}}</option>
            @empty
            <option value="">Tidak ada</option>
            @endforelse
        </select>
    </div>
    <br> <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection