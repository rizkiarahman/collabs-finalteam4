@extends('layout.master')
@include('layout.componen.navbar')
@section('judul')
  Halaman Index Hotel
@endsection
@section('content')

<div class="row">
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Bagaimana saya bisa memesan hotel?</h5>
        <p class="card-text">Proses pemesanan hotel radissonhotel.com mudah dan dengan sekali klik saja. Pilih tujuan Anda, masukkan tanggal check-in dan check-out. Lalu klik "Cari Sekarang!" 
          dan daftar pilihan Anda akan ditampilkan.</p>
        <a href="#" class="btn btn-primary">Cari Sekarang!</a>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Bagaimana saya bisa membatalkan pemesanan?</h5>
        <p class="card-text">Anda bisa membatalkan pemesanan Anda secara langsung di "Edit Pemesanan Saya". Perlu diperhatikan bahwa pembatalan, pengubahan atau pengembalian uang dibatasi oleh kebijakan hotel. Setelah dikonfirmasi, beberapa pemesanan tidak dapat dibatalkan, diubah atau diuangkan kembali.</p>
        <a href="#" class="btn btn-primary">Edit Pemesanan Saya</a>
      </div>
    </div>
  </div>
</div>




@endsection
