@extends('layout.master-2')
@section('judul')
  Halaman Tambah Tamu
@endsection
@section('content')

<form action="/tamu" method="POST">
    @csrf
    <div class="mb-3">
        <label class="form-label">Nama</label>
        <input type="text" name="nama" class="form-control">
    </div>
    <div class="mb-3">
        <label class="form-label">Alamat</label>
        <input type="text" name="alamat" class="form-control">
    </div>
    <div class="mb-3">
        <label class="form-label">Telpon</label>
        <input type="text" name="telp" class="form-control">
    </div>
    <div class="mb-3">
        <label class="form-label">Lama Inap</label>
        <input type="text" name="lama_inap" class="form-control">
    </div>
    <div class="mb-3">
        <label class="form-label">Status</label>
        <input type="text" name="status" class="form-control">
    </div>
    <div>
        <label class="form-label">transaksi</label>
        <select name="cast_id" class="form-control">
            <option value="">Pilih  ---</option>
            @forelse($transaksi as $item)
            <option value="{{$item->id}}">{{$item->reservasi}}</option>
            @empty
            <option value="">Tidak ada</option>
            @endforelse
        </select>
    </div>
    <br> <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection