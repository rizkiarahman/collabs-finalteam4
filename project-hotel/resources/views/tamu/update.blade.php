@extends('layout.master-2')
@section('judul')
  Halaman Update Tamu
@endsection
@section('content')

<form action="/tamu/{{$tamu->id}}" method="POST">
  @csrf
  @method('put')


    <div class="mb-3">
        <label class="form-label">Nama</label>
        <input type="text" name="nama" value="{{old ('nama',$tamu->nama)}}"class="form-control">
    </div>
    <div class="mb-3">
        <label class="form-label">Alamat</label>
        <input type="text" name="alamat" value="{{old ('alamat',$tamu->alamat)}}" class="form-control">
    </div>
    <div class="mb-3">
        <label class="form-label">Telpon</label>
        <input type="text" name="telp" value="{{old ('telp',$tamu->telp)}}" class="form-control">
    </div>
    <div class="mb-3">
        <label class="form-label">Lama Inap</label>
        <input type="text" name="lama_inap" value="{{old ('lama_inap',$tamu->lama_inap)}}" class="form-control">
    </div>
    <div class="mb-3">
        <label class="form-label">Status</label>
        <input type="text" name="status" value="{{old ('status',$tamu->status)}}" class="form-control">
    </div>
    <div>
        <label class="form-label">transaksi</label>
        <select name="transaksi_inap_id" class="form-control">
        <option value="">Pilih Reservasi ---</option>
            @forelse($transaksi as $item)
            @if($item->id===$tamu->cast_id)
            <option value="{{$item->id}}" selected>{{$item->reservasi}}</option>
            @else
            <option value="{{$item->id}}">{{$item->reservasi}}</option>
            @endif
            @empty
            <option value="">Tidak ada</option>
            @endforelse
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection