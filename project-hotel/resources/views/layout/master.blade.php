<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="{{ asset('landing/img/favicon.png') }}" type="image/png">
	<title>@yield('judul')</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="{{ asset('landing/css/bootstrap.css') }}">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="{{ asset('landing/vendors/linericon/style.css') }}">
	<link rel="stylesheet" href="{{ asset('landing/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('landing/vendors/owl-carousel/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ asset('landing/vendors/lightbox/simpleLightbox.css') }}">
	<link rel="stylesheet" href="{{ asset('landing/vendors/nice-select/css/nice-select.css') }}">
	<link rel="stylesheet" href="{{ asset('landing/vendors/animate-css/animate.css') }}">
	<link rel="stylesheet" href="{{ asset('landing/vendors/popup/magnific-popup.css') }}">
	<!-- main css -->
	<link rel="stylesheet" href="{{ asset('landing/css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('landing/css/responsive.css') }}">
    @stack('style')
</head>	
@include('layout.componen.navbar')
@yield('content')
    
    <!--================Home Banner Area =================-->
	<section class="home_banner_area">
		<div class="banner_inner d-flex align-items-center">
			<div class="overlay bg-parallax" data-stellar-ratio="2" data-stellar-vertical-offset="0" data-background=""></div>
			<!-- <div class="overlay overlay-bg"></div> -->
			<div class="container">
				<div class="banner_content text-center">
					<p class="top-text">Welcome to</p>
					<h1>Lorem</h1>
					<p class="text">If you are looking at blank cassettes on the web, you may be very confused at the difference in
						price. You may
						see some for as low as $.17 each.</p>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->

	<!--================Booking Area =================-->
	<section class="container">
		<div class="booking_area">
			<form action="">
				<div class="row">
					<div class="col-lg-3 col-sm-6 col-6">
						<div class="booking_item">
							<p>Check - in</p>
							<span class="day"></span>
							<span class="month">th sep</span>
							<label for="CheckIn">
								<i class="fa fa-angle-down"></i>
							</label>
							<input type="date" class="form-control" name='CheckIn' id="CheckIn" aria-describedby="emailHelp">
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 col-6">
						<div class="booking_item">
							<p>Check - out</p>
							<span class="day">25</span>
							<span class="month">th sep</span>
							<label for="CheckOut">
								<i class="fa fa-angle-down"></i>
							</label>
							<input type="date" class="form-control" name='CheckOut' id="CheckOut" aria-describedby="emailHelp">
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 col-6">
						<div class="booking_item">
							<p>Toatal guests</p>
							<span class="day">05</span>
							<span class="month">person</span>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 col-6 coupon-code">
						<div class="booking_item">
							<p class="text-capitalize">
								<a href="#">Got a Coupon Code?</a>
							</p>
							<button type="submit" class="main_btn text-uppercase">Book Now</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</section>
	<!--================End Booking Area =================-->

	<!--================About Area =================-->
	<section class="about_area section_gap">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="main_title">
						<div class="top-part">
							<p>About Us Our History Mission & Vision</p>
						</div>
						<h2>
							About Us Our History <br>
							Mission & Vision
						</h2>
						<div class="bottom_part">
							<p>
								inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards
								especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond
								reproach.
								inappropriate behavior is often laughed.
							</p>
						</div>
						<a href="#" class="main_btn mt-45">Request Custom Price</a>
					</div>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-lg-10">
					<div class="video_area" id="video">
						<img class="img-fluid" src="img/video-1.jpg" alt="">
						<div class="overlay overlay-bg"></div>
						<a class="popup-youtube" href="https://www.youtube.com/watch?v=VufDd-QL1c0">
							<img src="{{ asset('landing/img/video-icon-1.png') }}" alt="">
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End About Area =================-->

	<!-- Start Our Room Area -->
	<section class="our_room_area">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="main_title">
						<div class="top-part">
							<p>Our Rooms</p>
						</div>
						<h2>Our Rooms</h2>
						<div class="bottom_part">
							<p>
								We all live in an age that belongs to the young at heart. Life that is becoming extremely fast, day to day,
								also asks us to remain physically young. Young at heart,
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row align-items-center">
				<div class="col-lg-6">
					<div class="room_left">
						<img class="img-fluid" src="{{asset('landing/img/room/r1.jpg')}}" alt="">
					</div>
				</div>
				<div class="col-lg-6">
					<div class="owl-carousel owl-room">
						<div class="room_right">
							<h1 class="price">
								<i class="fa fa-dollar"></i>250<span>/night</span>
							</h1>
							<h1 class="type">
								Double Deluxe Room
							</h1>
							<p>
								We all live in an age that belongs to the young at heart. Life that is becoming extremely fast, day to day,
								also
								asks us to remain physically young. Young at heart,
							</p>
							<div class="row">
								<div class="col-lg-6 col-md-5">
									<ul>
										<li>Air Condition</li>
										<li>Car Parking</li>
										<li>Swimming Pool</li>
									</ul>
								</div>
								<div class="col-lg-6 col-md-5">
									<ul>
										<li>Restaurant & Bar</li>
										<li>Vehicle Rental</li>
										<li>Complementary Meal</li>
									</ul>
								</div>
							</div>
							<a href="#" class="main_btn">Book Now</a>
						</div>

						<div class="room_right">
							<h1 class="price">
								<i class="fa fa-dollar"></i>250<span>/night</span>
							</h1>
							<h1 class="type">
								Double Deluxe Room
							</h1>
							<p>
								We all live in an age that belongs to the young at heart. Life that is becoming extremely fast, day to day,
								also
								asks us to remain physically young. Young at heart,
							</p>
							<div class="row">
								<div class="col-lg-6 col-md-5">
									<ul>
										<li>Air Condition</li>
										<li>Car Parking</li>
										<li>Swimming Pool</li>
									</ul>
								</div>
								<div class="col-lg-6 col-md-5">
									<ul>
										<li>Restaurant & Bar</li>
										<li>Vehicle Rental</li>
										<li>Complementary Meal</li>
									</ul>
								</div>
							</div>
							<a href="#" class="main_btn">Book Now</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Our Room Area -->

	<!--================Testimonials Area =================-->
	<section class="testimonials-area section_gap_top color-bg">
		<div class="container">
			<div class="text-center">
				<img class="quote-img" src="img/quote.png" alt="">
			</div>
			<div class="testi-slider owl-carousel" data-slider-id="1">
				<div class="item">
					<div class="testi-item">
						<h4>Fanny Spencer</h4>
						<ul class="list">
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
						</ul>
						<div class="wow fadeIn" data-wow-duration="1s">
							<p>
								As conscious traveling Paup ers we must always be oncerned about our dear Mother Earth. If you think about it,
								you travel
								across her face <br> and She is the host to your journey.
							</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="testi-item">
						<h4>Fanny Spencer</h4>
						<ul class="list">
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
						</ul>
						<div class="wow fadeIn" data-wow-duration="1s">
							<p>
								As conscious traveling Paup ers we must always be oncerned about our dear Mother Earth. If you think about it,
								you travel
								across her face <br> and She is the host to your journey.
							</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="testi-item">
						<h4>Fanny Spencer</h4>
						<ul class="list">
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
						</ul>
						<div class="wow fadeIn" data-wow-duration="1s">
							<p>
								As conscious traveling Paup ers we must always be oncerned about our dear Mother Earth. If you think about it,
								you travel
								across her face <br> and She is the host to your journey.
							</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="testi-item">
						<img src="{{ asset('lading/img/quote.png') }}" alt="">
						<h4>Fanny Spencer</h4>
						<ul class="list">
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
						</ul>
						<div class="wow fadeIn" data-wow-duration="1s">
							<p>
								As conscious traveling Paup ers we must always be oncerned about our dear Mother Earth. If you think about it,
								you travel
								across her face <br> and She is the host to your journey.
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="owl-thumbs d-flex justify-content-center" data-slider-id="1">
				<div class="owl-thumb-item">
					<div>
						<img class="img-fluid" src="{{ asset('img/testimonial/t1.png') }}" alt="">
					</div>
					<div class="overlay overlay-grad"></div>
				</div>
				<div class="owl-thumb-item">
					<div>
						<img class="img-fluid" src="{{ asset('img/testimonial/t2.png') }}" alt="">
					</div>
					<div class="overlay overlay-grad"></div>
				</div>
				<div class="owl-thumb-item">
					<div>
						<img class="img-fluid" src="{{ asset('img/testimonial/t3.png') }}" alt="">
					</div>
					<div class="overlay overlay-grad"></div>
				</div>
				<div class="owl-thumb-item">
					<div>
						<img class="img-fluid" src="{{ asset('img/testimonial/t4.png') }}" alt="">
					</div>
					<div class="overlay overlay-grad"></div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Testimonials Area =================-->

	<!--================Latest Blog Area =================-->
	<section class="latest_blog_area section_gap color-bg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="main_title">
						<p class="text-uppercase">Our Blog Posts</p>
						<h2>
							Our Blog Posts
						</h2>
						<p>
							nappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards
							especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach.
							inappropriate behavior is often laughed.
						</p>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="single-recent-blog col-lg-4 col-md-6">
					<div class="thumb">
						<img class="f-img img-fluid mx-auto" src="{{ asset('landing/img/b1.jpg') }}" alt="">
					</div>
					<div class="bottom d-flex justify-content-between align-items-center flex-wrap">
						<div>
							<a href="#" class="main_btn">Travel</a>
						</div>
						<div class="meta">
							<span>30th Sep, 2018</span>
							<span>02 Comments</span>
						</div>
					</div>
					<a href="#">
						<h4>Low Cost Advertising let us know
							the side effect of printing</h4>
					</a>
					<p>
						Acres of Diamonds… you’ve read the famous story, or at least had it related to you. A farmer hears tales of
						diamonds and begins dreaming of vast riches. He sells his farm and hikes off over the horizon never heard.
					</p>
				</div>
				<div class="single-recent-blog col-lg-4 col-md-6">
					<div class="thumb">
						<img class="f-img img-fluid mx-auto" src="{{ ('landing/img/b2.jpg')}}" alt="">
					</div>
					<div class="bottom d-flex justify-content-between align-items-center flex-wrap">
						<div>
							<a href="#" class="main_btn">Travel</a>
						</div>
						<div class="meta">
							<span>30th Sep, 2018</span>
							<span>02 Comments</span>
						</div>
					</div>
					<a href="#">
						<h4>Low Cost Advertising let us know
							the side effect of printing</h4>
					</a>
					<p>
						Acres of Diamonds… you’ve read the famous story, or at least had it related to you. A farmer hears tales of
						diamonds and begins dreaming of vast riches. He sells his farm and hikes off over the horizon never heard.
					</p>
				</div>
				<div class="single-recent-blog col-lg-4 col-md-6">
					<div class="thumb">
						<img class="f-img img-fluid mx-auto" src="{{ asset('landing/img/b3.jpg') }}" alt="">
					</div>
					<div class="bottom d-flex justify-content-between align-items-center flex-wrap">
						<div>
							<a href="#" class="main_btn">Travel</a>
						</div>
						<div class="meta">
							<span>30th Sep, 2018</span>
							<span>02 Comments</span>
						</div>
					</div>
					<a href="#">
						<h4>Low Cost Advertising let us know
							the side effect of printing</h4>
					</a>
					<p>
						Acres of Diamonds… you’ve read the famous story, or at least had it related to you. A farmer hears tales of
						diamonds and begins dreaming of vast riches. He sells his farm and hikes off over the horizon never heard.
					</p>
				</div>
			</div>
		</div>
	</section>
    	<script src="{{ asset('landing/js/jquery.min.js') }}"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="{{ asset('landing/js/popper.js') }}"></script>
	<script src="{{ asset('landing/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('landing/js/stellar.js') }}"></script>
	<script src="{{ asset('landing/vendors/lightbox/simpleLightbox.min.js') }}"></script>
	<script src="{{ asset('landing/vendors/nice-select/js/jquery.nice-select.min.js') }}"></script>
	<script src="{{ asset('landing/vendors/isotope/imagesloaded.pkgd.min.js') }}"></script>
	<script src="{{ asset('landing/vendors/isotope/isotope.pkgd.min.js') }}"></script>
	<script src="{{ asset('landing/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('landing/js/owl-carousel-thumb.min.js') }}"></script>
	<script src="{{ asset('landing/vendors/popup/jquery.magnific-popup.min.js') }}"></script>
	<script src="{{ asset('landing/js/jquery.ajaxchimp.min.js') }}"></script>
	<script src="{{ asset('landing/vendors/counter-up/jquery.waypoints.min.js') }}"></script>
	<script src="{{ asset('landing/vendors/counter-up/jquery.counterup.js') }}"></script>
	<script src="{{ asset('landing/js/mail-script.js') }}"></script>
	<script src="{{ asset('landing/js/theme.js') }}"></script>
    @stack('script')
    @include('layout.componen.footer')
    