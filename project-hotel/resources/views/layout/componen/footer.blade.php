<footer class="footer-area section_gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-3  col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6>About Agency</h6>
                    <p>
                        The world has become so fasted that people don’t want to standby reading page of info they would much rather
                        look at a presentation and understand message.
                    </p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6>Quick Links</h6>
                    <div class="row">
                        <ul class="col footer-nav">
                            <li><a href="#">Sitemaps</a></li>
                            <li><a href="#">Categories</a></li>
                            <li><a href="#">Archives</a></li>
                            <li><a href="#">Advertise</a></li>
                            <li><a href="#">Ad Choice</a></li>
                        </ul>
                        <ul class="col footer-nav">
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms of Use</a></li>
                            <li><a href="#">Help Center</a></li>
                            <li><a href="#">Newsletters</a></li>
                            <li><a href="#">Feedback</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-lg-3  col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6>Newsletter</h6>
                    <p>For business professionals caught between high OEM price mediocre print and graphic.</p>
                    <div class="" id="mc_embed_signup">

                        <form target="_blank" novalidate="true" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get" class="form-inline">

                            <div class="d-flex flex-row">

                                <input class="form-control" name="EMAIL" placeholder="Enter Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email '" required="" type="email">


                                <button class="click-btn btn btn-default"><i class="lnr lnr-location" aria-hidden="true"></i></button>
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>

                                <!-- <div class="col-lg-4 col-md-4">
                                    <button class="bb-btn btn"><span class="lnr lnr-arrow-right"></span></button>
                                </div>  -->
                            </div>
                            <div class="info"></div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-3  col-md-6 col-sm-6">
                <div class="single-footer-widget mail-chimp">
                    <h6 class="mb-20">Instafeed</h6>
                    <ul class="instafeed d-flex flex-wrap">
                        <li><img src="img/instagram/Image-01.jpg" alt=""></li>
                        <li><img src="img/instagram/Image-02.jpg" alt=""></li>
                        <li><img src="img/instagram/Image-03.jpg" alt=""></li>
                        <li><img src="img/instagram/Image-04.jpg" alt=""></li>
                        <li><img src="img/instagram/Image-05.jpg" alt=""></li>
                        <li><img src="img/instagram/Image-06.jpg" alt=""></li>
                        <li><img src="img/instagram/Image-07.jpg" alt=""></li>
                        <li><img src="img/instagram/Image-08.jpg" alt=""></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-bottom d-flex justify-content-between align-items-center flex-wrap">
            <div>
                <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright ©<script>document.write(new Date().getFullYear());</script>2022 All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            </div>
            <div class="footer-social d-flex align-items-center">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-dribbble"></i></a>
                <a href="#"><i class="fa fa-behance"></i></a>
            </div>
        </div>
    </div>
</footer>