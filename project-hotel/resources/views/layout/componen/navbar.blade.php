<header class="header_area">
    <div class="top_menu row m0">
        <div class="container">
            <div class="float-left">
                <ul class="list header_social mr-2">
                    <li><a href="#">Contact Us +44 (012) 5689 3264</a></li>
                 
                        <a href="#"><i class="fa fa-facebook"></i>    Lorem ipsum dolor sit.</a>
                        <a href="#"><i class="fa fa-twitter"></i>   Ipsam.</a>
                </ul>
            </div>
        </div>
    </div>
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <a class="navbar-brand logo_h" href="index.html">HOTEL LOREM</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav ml-auto">
                        <li class="nav-item active"><a class="nav-link" href="index.html">Home</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ ('/tamu') }}">Guest</a></li>
                        <li class="nav-item"><a class="nav-link" href="gallery.html">Gallery</a></li>
                        <li class="nav-item"><a href="#" class="nav-link">Rooms</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ ('/login') }}">Login</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>