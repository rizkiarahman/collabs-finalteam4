<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdFasilitasTransaksiplgToTransaksiInap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaksi_inap', function (Blueprint $table) {
            $table->unsignedBigInteger('transaksi_pulang_id');
            $table->foreign('transaksi_pulang_id')
                ->references('id')->on('transaksi_pulang')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->unsignedBigInteger('fasilitas_id');
            $table->foreign('fasilitas_id')
                ->references('id')->on('fasilitas')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaksi_inap', function (Blueprint $table) {
            //
        });
    }
}
