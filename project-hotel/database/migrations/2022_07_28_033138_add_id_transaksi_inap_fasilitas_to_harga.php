<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdTransaksiInapFasilitasToHarga extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('harga', function (Blueprint $table) {
           
            $table->unsignedBigInteger('transaksi_inap_id');
            $table->foreign('transaksi_inap_id')
                ->references('id')->on('transaksi_inap')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->unsignedBigInteger('fasilitas_id');
            $table->foreign('fasilitas_id')
                ->references('id')->on('fasilitas')
                ->onDelete('cascade')
                ->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('harga', function (Blueprint $table) {
            //
        });
    }
}
