<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
use App\Tamu;
use App\transaksi;
use App\Fasilitas;
use App\Transaksipulang;
use Symfony\Component\HttpKernel\Profiler\Profile;

class fasilitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fasilitas = fasilitas::all();
        return view('fasilitas.index', compact('fasilitas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          return view('fasilitas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'jumlah_kamar' => 'required',
             'type_kamar' => 'required',
            'fasilitas_kamar'=>'required',
        ]);
        $fasilitas = new Fasilitas;
        $fasilitas->jumlah_kamar = $request->jumlah_kamar;
        $fasilitas->type_kamar = $request->type_kamar;
        $fasilitas->fasilitas_kamar = $request->fasilitas_kamar;
       
        $fasilitas->save();
        return redirect('/fasilitas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fasilitas = fasilitas::find($id);
        return view('fasilitas.detail', compact('fasilitas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //edit fasilitas
        $fasilitas = fasilitas::find($id);
        return view('fasilitas.update', compact('fasilitas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
                 $request->validate([
            'jumlah_kamar' => 'required',
             'type_kamar' => 'required',
            'fasilitas_kamar'=>'required',
        ]);
        $fasilitas = new Fasilitas;
        $fasilitas->jumlah_kamar = $request->jumlah_kamar;
        $fasilitas->type_kamar = $request->type_kamar;
        $fasilitas->fasilitas_kamar = $request->fasilitas_kamar;
        $fasilitas->save();
        return redirect('/fasilitas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $fasilitas = new Fasilitas;
        $fasilitas->Fasilitas::find($id);
        $fasilitas->delete();
        return redirect('/fasilitas');
    }
}
