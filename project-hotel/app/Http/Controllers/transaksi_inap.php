<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
use App\Tamu;
use App\transaksi;
use App\Fasilitas;
use App\Transaksipulang;
use Symfony\Component\HttpKernel\Profiler\Profile;

class transaksi_inap extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksi = transaksi::all();
        return view('transaksi_inap.index'); 
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Transaksipulang = Transaksipulang::all();
        $fasilitas = fasilitas::all();
        return view('transaksi_inap.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //untuk transaksi_inap

        $request->validate([
            'reservasi' => 'required',
             'tgl_checkin' => 'required',
             'transaksi_pulang_id' => 'required',
             'fasilitas_id'=>'required',
        ]);
        $transaksi = new transaksi;
        $transaksi->reservasi = $request->reservasi;
        $transaksi->tgl_checkin = $request->tgl_checkin;
        $transaksi->transaksi_pulang_id = $request->transaksi_pulang_id;
        $transaksi->fasilitas_id = $request->fasilitas_id;
       
        $transaksi->save();
        return redirect('/transaksi_inap');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('transaksi_inap.detail', compact('transaksi_inap'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $Transaksipulang = Transaksipulang::all();
        $fasilitas = fasilitas::all();
        $transaksi = transaksi::find($id);
        return view('transaksi_inap.update', compact('transaksi_pulang', 'fasilitas','transaksi_inap'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $request->validate([
            'reservasi' => 'required',
             'tgl_checkin' => 'required',
            'transaksi_pulang_id' => 'required',
            'fasilitas_id'=>'required',
        ]);
        $transaksi=new transaksi;
        $transaksi->reservasi = $request->reservasi;
        $transaksi->tgl_checkin = $request->tgl_checkin;
        $transaksi->transaksi_pulang_id = $request->transaksi_pulang_id;
        $transaksi->fasilitas_id = $request->fasilitas_id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaksi = new transaksi;
        $transaksi->transaksi::find($id);
        $transaksi->delete();
        return redirect('/transaksi_inap');
    }
}
