<?php


namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
use App\Tamu;
use App\transaksi;
use App\Fasilitas;
use App\Kamar;
use App\Profil;
use App\Transaksipulang;
use App\Users;
use Symfony\Component\HttpKernel\Profiler\Profile;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tamu = tamu::all();
        return view('tamu.index', compact('tamu'));
        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $transaksi = transaksi::all();
        return view('tamu.create', compact('transaksi'));     
    }

  

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
             'alamat' => 'required',
            'telp' => 'required',
            'lama_inap'=>'required',
            'status'=>'required',
            'transaksi_inap_id'=>'',
        ]);
        $tamu = new Tamu;
        $tamu->nama = $request->nama;
        $tamu->alamat = $request->alamat;
        $tamu->telp = $request->telp;
        $tamu->lama_inap = $request->lama_inap;
        $tamu->status = $request->status;
        $tamu->transaksi_inap_id = $request->transaksi_inap_id;


        $tamu->save();
        return redirect('/tamu');



         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tamu = tamu::find($id);
        return view('tamu.detail', compact('tamu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaksi = transaksi::all();
        $tamu = tamu::find($id);
        return view('tamu.update', compact('transaksi', 'tamu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
             'alamat' => 'required',
            'telp' => 'required',
            'lama_inap'=>'required',
            'status'=>'required',
            'transaksi_inap_id'=>'required',
        ]);
        $tamu = new tamu;
        $tamu->nama = $request->nama;
        $tamu->alamat = $request->alamat;
        $tamu->telp = $request->telp;
        $tamu->lama_inap = $request->lama_inap;
        $tamu->status = $request->status;
        $tamu->transaksi_inap_id = $request->transaksi_inap_id;


        $tamu->save();
        return redirect('/tamu');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tamu = new tamu;
        $tamu->tamu::find($id);
        $tamu->delete();
        return redirect('/tamu');

    }
}

