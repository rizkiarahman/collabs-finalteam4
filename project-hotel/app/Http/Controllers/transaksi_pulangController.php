<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
use App\Tamu;
use App\transaksi;
use App\Fasilitas;
use App\Transaksipulang;
use Symfony\Component\HttpKernel\Profiler\Profile;

class transaksi_pulangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Transaksipulang = Transaksipulang::all();
        return view('transaksi_pulang.index', compact('transaksi_pulang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transaksi_pulang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'tgl_checkout' => 'required',
        ]);
        $Transaksipulang = new Transaksipulang;
        $Transaksipulang->tgl_checkout = $request->tgl_checkout;
        
        $Transaksipulang->save();
        return redirect('/transaksi_pulang');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('transaksi_pulang.detail', compact('transaksi_pulang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Transaksipulang = Transaksipulang::find($id);
        return view('transaksi_pulang.update', compact('transaksi_pulang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
            'tgl_checkout' => 'required',
        ]);
        $Transaksipulang = new Transaksipulang;
        $Transaksipulang->tgl_checkout = $request->tgl_checkout;
        
        $Transaksipulang->save();
        return redirect('/transaksi_pulang');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Transaksipulang = newTransaksipulang;
        $Transaksipulang->Transaksipulang::find($id);
        $Transaksipulang->delete();
        return redirect('/transaksi_pulang');
    }
}
