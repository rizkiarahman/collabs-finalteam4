<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksipulang extends Model
{
    protected $table = 'transaksi_pulang';
    protected $fillable = [
        'tgl_checkout'
    ];
}
