<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kamar extends Model
{
    protected $table = 'kamar';
    protected $fillable = [
        
        'no_kamar',
        'gambar',
        'fasilitas_id'
       ];
}
