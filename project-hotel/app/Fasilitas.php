<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fasilitas extends Model
{
    protected $table = 'fasilitas';
    protected $fillable = [
        'jumlah_kamar',
        'type_kamar',
        'fasilitas_kamar'
    ];
}
