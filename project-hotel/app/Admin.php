<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class admin extends Model
{
    //
    protected $table = 'admin';
    protected $fillable = ['user_id'];
}

class fasilitas extends Model
{
    protected $table = 'fasilitas';
    protected $fillable = ['jumlah_kamar', 'tipe_kamar', 'fasilitas_kamar'];
}

class harga extends Model
{
    protected $table = 'harga';
    protected $fillable = ['weekday', 'weekend', 'transaksi_inap_id', 'fasilitas_id'];
}

class kamar extends Model
{
    protected $table = 'kamar';
    protected $fillable = ['no_kamar', 'gambar', 'fasilitas_id'];
}

class profil extends Model
{
    protected $table = 'profile';
    protected $fillable = ['gambar', 'user_id', 'tamu_id'];
}


class transaksi_pulang extends Model
{

    protected $table = 'transaksi_pulang';
    protected $fillable = ['tgl_checkout'];
}

class user extends Model
{

    protected $table = 'users';
    protected $fillable = ['username', 'email', 'email_verified_at', 'password', 'remember_token'];
}
