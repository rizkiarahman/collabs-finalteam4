<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaksi extends Model
{
    protected $table = 'transaksi_inap';
    protected $fillable = ['reservasi', 'tgl_checkin', 'transaksi_pulang_id', 'fasilitas_id'];
}
